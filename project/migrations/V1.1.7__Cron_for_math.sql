create extension pg_cron;

-- refresh представления каждую минуту:
select cron.schedule('refresh_client_with_calls', '* * * * *',
                     $$ refresh materialized view concurrently client_with_calls $$);

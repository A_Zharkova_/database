-- начало транзакции
begin;

drop materialized view if exists client_with_calls cascade;

-- подключаем функцию для генерации uuid (по-умолчанию отключена)

create extension if not exists "uuid-ossp";


-- 1) из таблицы, которая ссылаются на client, удаляем constraint с внешним ключом
alter table info_calls
    drop constraint info_calls_who_calls_id_fkey;

alter table info_calls
    drop constraint info_calls_whom_calls_id_fkey;


-- 2) переименовываем колонку со старым внешним ключом
alter table info_calls
    rename column who_calls_id to old_who_calls_id;

alter table info_calls
    rename column whom_calls_id to old_whom_calls_id;

-- 3) добавляем колонку с новым внешним ключом
alter table info_calls
    add column who_calls_id uuid;

alter table info_calls
    add column whom_calls_id uuid;


-- в таблице client удаляем constraint primary key
alter table client
    drop constraint client_pkey;

-- переименовываем колонку со старым ключом
alter table client
    rename column id to old_id;

-- добавляем колонку с новым ключом
alter table client
    add column id uuid default uuid_generate_v4();

/*
анонимная функция, которая циклом обходит все строки таблицы client,
для каждой строки добавляет новый id, и всем ссылающимся на эту строку
строкам из других таблиц проставляет новый id
*/
do
$$
    declare
        row record;
    begin
        for row in select * from client
            loop
                update info_calls set who_calls_id = row.id where old_who_calls_id = row.old_id;
                update info_calls set whom_calls_id = row.id where old_whom_calls_id = row.old_id;
            end loop;
    end
$$;

-- удаляем столбец со старым id
alter table client
    drop column old_id;
-- устанавливаем первичный ключ
alter table client
    add primary key (id);

-- удаляем столбец со старым внешним ключом
-- удаление этой колонки так же удалит установленное ранее ограничение на уникальность
alter table info_calls
    drop column old_who_calls_id;

alter table info_calls
    drop column old_whom_calls_id;

-- устанавливаем новый внешний ключ
alter table info_calls
    add constraint fk_who_calls_id foreign key (who_calls_id) references client;

alter table info_calls
    add constraint fk_whom_calls_id foreign key (whom_calls_id) references client;

-- для связи many-to-many ставим соответствующие ограничения
alter table info_calls
    alter column who_calls_id set not null;

alter table info_calls
    alter column whom_calls_id set not null;

alter table info_calls
    add constraint uq_who_calls_id_to_whom_calls_id unique (who_calls_id, whom_calls_id);



-------------------------------------------------------------------------------------------------------------
create materialized view client_with_calls as
select who.id as who_id, who.name as who_calls_name, who.phone as phone, who.address,
       whom.id as whom_id, whom.name as whom_calls_name, ic.id as ic_id, avg_call_duration, date_call
from client who
         join info_calls ic on who.id = ic.who_calls_id
         join client whom on whom.id = ic.whom_calls_id;

create unique index client_with_calls_id on client_with_calls (who_id, whom_id, ic_id);

refresh
    materialized view concurrently client_with_calls;
-------------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------------
create index materialized_search_by_id_address on client_with_calls
    using btree (phone, avg_call_duration, address);
-------------------------------------------------------------------------------------------------------------


-- завершение транзакции
commit;
drop function if exists refresh_tours_with_reviews;
create function refresh_client_with_calls()
    returns trigger as
$$
begin
    refresh materialized view concurrently client_with_calls;

    return new;
end;
$$
    language 'plpgsql';

create trigger update_info_calls_table
    after insert or update or delete
    on info_calls
    for each row
execute function refresh_client_with_calls();

drop table if exists client, info_calls cascade;

create table client
(
    id       int generated always as identity primary key,
    name     text,
    age      int,
    phone    text,
    address  text,
    birthday text,
    email    text
);

create table info_calls
(
    id  int generated always as identity primary key,
    who_calls_id    int references client not null,
    whom_calls_id   int references client not null,
    count_of_calls   int,
    avg_call_duration   int,
    date_call       text,
    client_category text,
    digitized_text  text
);

insert into client(name, age, phone, address, birthday, email)
values ('Anastasia', 19, '8910540942', 'Moscow', '03.04.2003', 'nastya.zharkova.03@bk.ru'),
       ('Katya', 19, '89125437865', 'Sochi', '28.11.2003', 'ekaterina03@gmail.com'),
       ('Egor', 22, '89567841745', 'Volgograd', '11.06.1999', 'e_gor2000@yandex.ru'),
       ('Lera', 20, '89106739257', 'Sochi', '15.02.2004', 'lera.0206@mail.ru'),
       ('Arina', 35, '89564447211', 'Kaluga', '11.07.2003', 'arina_bivsheva19@gmail.com'),
       ('Sasha', 55, '89153450906', 'Moscow', '29.06.2004', 'a_aleksandr2001@bk.ru');

insert into info_calls(who_calls_id, whom_calls_id, count_of_calls, avg_call_duration, date_call, client_category, digitized_text)
values (1, 2, 4, 60, '01.11.2021', 'Родственник', 'Привет! Во сколько сегодня встретимся? - Давай в 4'),
       (1, 3, 2, 60, '01.11.2021', 'Родственник', 'Где продают ту вещь, про которую ты мне вчера говорил?'),
       (5, 6, 5, 120, '11.06.2018', 'Родственник', 'Где ты?! Жду тебя уже 20 минут!!!'),
       (4, 3, 1, 200, '28.02.2020', 'Друг', 'Как мне найти человека который точно знает про это всё'),
       (2, 4, 6, 50, '13.05.2021', 'Коллега', 'Добрый вечер! Когда я могу получить свой товар? -Здравствуйте! Сегодня в 23.00'),
       (3, 1, 7, 120, '12.07.2020', 'Друг', 'Привет! Когда мы с тобой уже пойдём в кино???'),
       (6, 5, 1, 80, '13.07.2020', 'Коллега', 'Добрый день! Когда будет готов отчёт по проделанной работе?');
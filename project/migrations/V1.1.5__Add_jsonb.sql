alter table client
	drop column if exists family;

alter table client
	add column family jsonb;

update client
    set family = (case when name = 'Anastasia' then '[{
                                    "name": "Елена",
                                    "phone": "89802345162",
                                    "family_ties": "Мать",
                                    "age": "39"
								},
                                {
                                    "name": "Никита",
                                    "phone": "89204765324",
                                    "family_ties": "Брат",
                                    "age": "16"
                                }]'::jsonb

                       when name = 'Katya' then '[{
                                    "name": "Дмитрий",
                                    "phone": "89623452176",
                                    "family_ties": "Отец",
                                    "age": "35"
								},
                                {
                                    "name": "Олеся",
                                    "phone": "89345672132",
                                    "family_ties": "Сестра",
                                    "age": "12"
                                }]'::jsonb

                       when name = 'Egor' then '[{
                                    "name": "Светлана",
                                    "phone": "89783674523",
                                    "family_ties": "Тётя",
                                    "age": "30"
								},
                                {
                                    "name": "Александр",
                                    "phone": "89125673908",
                                    "family_ties": "Брат",
                                    "age": "21"
                                }]'::jsonb

                       when name = 'Lera' then '[{
                                    "name": "Татьяна",
                                    "phone": "89324107843",
                                    "family_ties": "Бабушка",
                                    "age": "65"
								},
                                {
                                    "name": "Игорь",
                                    "phone": "89234756721",
                                    "family_ties": "Дедушка",
                                    "age": "68"
                                }]'::jsonb

                       when name = 'Arina' then '[{
                                    "name": "Игорь",
                                    "phone": "89154362745",
                                    "family_ties": "Муж",
                                    "age": "40"
								},
                                {
                                    "name": "Кристина",
                                    "phone": "89104387643",
                                    "family_ties": "Дочь",
                                    "age": "9"
                                }]'::jsonb

                       when name = 'Sasha' then '[{
                                    "name": "Анастасия",
                                    "phone": "89564736209",
                                    "family_ties": "Жена",
                                    "age": "35"
								},
                                {
                                    "name": "Олег",
                                    "phone": "89576847531",
                                    "family_ties": "Сын",
                                    "age": "12"
                                }]'::jsonb


                end)
    where name in ('Anastasia', 'Katya', 'Egor', 'Lera', 'Arina', 'Sasha');

drop index if exists client_family;
create index client_family on client using gin (family);
drop materialized view if exists client_with_calls cascade;
drop index if exists client_search_by_birthday_address_phone, client_with_calls_id, materialized_search_by_id_address;

--индекс ----------------------------------------------------------------

create index client_search_by_birthday_address_phone on client
    using btree (birthday, address, name);

---------------------------------------------------------------------------


-- материализованное представление ----------------------------------------------------------------

create materialized view client_with_calls as
select who.id as who_id, who.name as who_calls_name, who.phone as phone, who.address,
       whom.id as whom_id, whom.name as whom_calls_name, ic.id as ic_id, avg_call_duration, date_call
from client who
         join info_calls ic on who.id = ic.who_calls_id
         join client whom on whom.id = ic.whom_calls_id;

create unique index client_with_calls_id on client_with_calls (who_id, whom_id, ic_id);

refresh
    materialized view concurrently client_with_calls;

----------------------------------------------------------------------------------------------------


--индекс materialized view --------------------------------------------------

create index materialized_search_by_id_address on client_with_calls
    using btree (phone, avg_call_duration, address);

------------------------------------------------------------------------------
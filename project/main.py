import json, logging, os, psycopg2, clickhouse_connect
from flask import Flask, jsonify, request, send_file
from psycopg2.extras import Json, RealDictCursor
from redis import Redis
from elasticsearch import Elasticsearch


app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False


def get_elastic_connection():
    host = os.getenv('ELASTIC_HOST') or '127.0.0.1'
    port = os.getenv('ELASTIC_PORT')

    return Elasticsearch(hosts=f'http://{host}:{port}')

def get_pg_connection():
    pg_conn = psycopg2.connect(host=os.getenv('POSTGRES_HOST') or '127.0.0.1', port=os.getenv('POSTGRES_PORT'),
                               database=os.getenv('POSTGRES_DB'), user=os.getenv('POSTGRES_USER'),
                               password=os.getenv('POSTGRES_PASSWORD'), cursor_factory=RealDictCursor)
    pg_conn.autocommit = True
    return pg_conn

def get_pg_con_rep():
    pg_conn_rep = psycopg2.connect(host=os.getenv('SLAVE_HOST'), port=os.getenv('SLAVE_PORT'),
                               database=os.getenv('POSTGRES_DB'), user=os.getenv('POSTGRES_USER'),
                               password=os.getenv('POSTGRES_PASSWORD'), cursor_factory=RealDictCursor)
    pg_conn_rep.autocommit = True
    return pg_conn_rep


def get_redis_connection():
    return Redis(host=os.getenv('REDIS_HOST') or '127.0.0.1', port=os.getenv('REDIS_PORT'),
                 password=os.getenv('REDIS_PASSWORD'), decode_responses=True)

def get_clickhouse_connection():
    host = os.getenv('CLICKHOUSE_HOST') or '127.0.0.1'
    port = os.getenv('CLICKHOUSE_PORT')
    user = os.getenv('CLICKHOUSE_USER')
    password = os.getenv('CLICKHOUSE_PASSWORD')

    return clickhouse_connect.get_client(host=host, username=user, password=password, port=port, database='postgres_repl')



@app.route('/clients')
def get_clients():
    try:
        offset = request.args.get('offset')
        limit = request.args.get('limit')
        redis_key = f'clients:offset={offset},limit={limit}'
        with get_redis_connection() as redis_conn:
            redis_clients = redis_conn.get(redis_key)

        if redis_clients is None:
            query = """
            select who.id as who_id, who.name as who_calls_name, who.phone as phone, who.address, 
              whom.id as whom_id, whom.name as whom_calls_name, ic.id as ic_id, call_duration, date_call
            from client who
            join info_calls ic on who.id = ic.who_calls_id
            join client whom on whom.id = ic.whom_calls_id
                order by who.id
                limit %s
                offset %s
            """

            with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
                cur.execute(query, (offset, limit))
                rows = cur.fetchall()

            redis_clients = json.dumps(rows, ensure_ascii=False, default=str, indent=2)

            with get_redis_connection() as redis_conn:
                redis_conn.set(redis_key, redis_clients, ex=30)

        return redis_clients, 200, {'content-type': 'text/json'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


@app.route('/client/create', methods=['POST'])
def create_clients():
    try:
        body = request.json
        name = body['name']
        phone = body['phone']
        address = body['address']
        birthday = body['birthday']
        email = body['email']

        query = f"""
        insert into client (name, phone, address, birthday, email)
        values (%s, %s, %s, %s, %s)
        returning name, phone, address, birthday, email
        """

        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (name, phone, address, birthday, email,))
            rows = cur.fetchall()

        return {'message': f'Client {rows[0]["name"]} with phone = {rows[0]["phone"]} created.'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


@app.route('/client/update', methods=['POST'])
def update_clients():
    try:
        body = request.json
        name = body['name']
        phone = body['phone']

        query = f"""
        update client
        set name = %s
        where phone = %s
        returning phone
        """

        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (name, phone,))
            rows = cur.fetchall()

        return {'message': f'Client with phone = {rows[0]["phone"]} updated.'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400

@app.route('/client/delete', methods=['DELETE'])
def delete_holder():
    try:
        body = request.json
        phone = body['phone']

        query = f"""
        delete from client
        where phone = %s
        returning phone
        """

        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (phone,))
            rows = cur.fetchall()

        return {'message': f'Client with phone {rows[0]["phone"]} deleted.'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400

# connection replica 
@app.route('/clients/histogram')
def get_histogram():
    try:
        query=f"""
        with max_time as (select max(call_duration) as time from info_calls),
            histogram as (select width_bucket(call_duration, 0, (select time from max_time), 9) as bucket,
                          count(*) as frequency
                   from info_calls
                   group by bucket)
        select bucket,
               frequency,
               (bucket - 1) * (select time / 10 from max_time) as range_from,
                bucket * (select time / 10 from max_time)       as range_to
        from histogram
        order by bucket;"""

        with get_pg_con_rep() as pg_conn_rep, pg_conn_rep.cursor() as cur:
            cur.execute(query)
            graph = cur.fetchall()

        redis_graph = json.dumps(graph, default=vars, ensure_ascii=False, indent = 2)

        return redis_graph, 200, {'content-type': 'text/json'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400

@app.route('/client/index_search', methods=['POST'])
def get_index_search():
    try:
        body = request.json
        address = body['address']


        query = f"""
            select name, birthday, address, phone
            from client
            where address = %s
            """           

        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (address,))
            rows = cur.fetchall()
   
        return {'message': f'Clients {rows[0]["name"]} live in {rows[0]["address"]}.'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400

@app.route('/clients/materialized')
def get_materialized():
    try:
        
        query=f"""
    select *
    from client_with_calls;"""


        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query)
            graph = cur.fetchall()

        redis_graph = json.dumps(graph, default=vars, ensure_ascii=False, indent = 2)

        return redis_graph, 200, {'content-type': 'text/json'}
    
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400

@app.route('/client/materialized_index', methods=['POST'])
def get_materialized_index():
    try:
        body = request.json
        address = body['address']
        call_duration = body['call_duration']

        query=f"""
            select  who_id, who_calls_name, phone, address, ic_id, date_call, call_duration
            from client_with_calls
            where address = %s and call_duration = %s
            """

        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
                cur.execute(query, (address, call_duration,))
                rows = cur.fetchall()

        return {'message': f'Client {rows[0]["who_calls_name"]} with call_duration = {rows[0]["call_duration"]} existed.'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400

@app.route('/client/jsonb', methods =['POST'])
def json_db():
    try:
        body = request.json
        name = body['name']
        query_param = [{"name": name}]

        query=f"""
            select *
            from client
            where family @> %s;
            """

        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
                cur.execute(query, (Json(query_param),))
                rows = cur.fetchall()

        return {'message': f'Client existed.'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400

@app.route('/client/array', methods =['POST'])
def array_db():
    try:
        body = request.json
        coordinates = body['coordinates']

        query=f"""
            select *
            from info_calls
            where coordinates && array [%s];
            """

        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
                cur.execute(query, (coordinates,))        

        return {'message': f'Client existed.'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400




### to Elastic
@app.route('/client/search_by_name', methods =['GET'])
def search_by_exist():
    try:

        name = request.args.get('name')

        query = {
            "term": {
                "name": name
            }
        }

        with get_elastic_connection() as es:
            es_resp = es.search(index='client', query=query)

        client = []
        for hit in es_resp['hits']['hits']:
            result = hit['_source']
            result.pop('_meta', None)
                
            client.append(result)

        return jsonify(client)

    except Exception as ex:
        return {'message': repr(ex)}, 400


@app.route('/info_calls/range_search', methods =['GET'])
def range_search():
    try:

        gte = request.args.get('avg_call_duration')
        lte = request.args.get('avg_call_duration')

        query = {
        "range": {
            "avg_call_duration": {
                "gte": gte,
                "lte": lte
            }
        }
        }

        with get_elastic_connection() as es:
            es_resp = es.search(index='info_calls', query=query)

        info_calls = []
        for hit in es_resp['hits']['hits']:
            result = hit['_source']
            result.pop('_meta', None)
                
            info_calls.append(result)

        return jsonify(info_calls)

    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


@app.route('/')
def autocomplete_page():
    try:
        return send_file('static/autocomplete.html')
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400

@app.route('/autocomplete')
def autocomplete():
    try:
        word = request.args.get('word')

        query = {
            "match": {
                "digitized_text": {
                    "query": word,
                    "analyzer": "my_ngram_analyzer"
                }
            }
        }

        with get_elastic_connection() as es:
            es_resp = es.search(index='info_calls', query=query)

        return jsonify(list(map(lambda hit: hit['_source']['digitized_text'], es_resp['hits']['hits'])))
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400

### to ClickHouse
@app.route('/info_calls/info_agg')
def search_by_info_aggs():
    try:
        query = """
        select postgres_repl.client.name,
       min(postgres_repl.info_calls.avg_call_duration)                                           as min_avg_call_duration,
       max(postgres_repl.info_calls.avg_call_duration)                                           as max_avg_call_duration,
       sum(postgres_repl.info_calls.count_of_calls),
       sum(postgres_repl.info_calls.count_of_calls * postgres_repl.info_calls.avg_call_duration) as total_duration,
       sum(postgres_repl.info_calls.count_of_calls * postgres_repl.info_calls.avg_call_duration) /
       sum(postgres_repl.info_calls.count_of_calls)                                              as weighted_duration

        from postgres_repl.info_calls
                join postgres_repl.client on postgres_repl.client.id = postgres_repl.info_calls.who_calls_id
        group by postgres_repl.client.name"""

        with get_clickhouse_connection() as ch:
            result = ch.query(query)
        json_info_calls = json.dumps(result.result_set, default=str, ensure_ascii=False, indent=2)
        return json_info_calls, 200, {'content-type': 'text/json'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


@app.route('/client/range_search')
def search_by_range():
    try:
        query = """
        select postgres_repl.client.name,
        postgres_repl.client.age,
        postgres_repl.client.phone,
        postgres_repl.client.address,
        postgres_repl.info_calls.client_category,
        postgres_repl.info_calls.digitized_text
    from postgres_repl.info_calls
            join postgres_repl.client on postgres_repl.client.id = postgres_repl.info_calls.who_calls_id
    where age > 25"""

        with get_clickhouse_connection() as ch:
            result = ch.query(query)
        json_client = json.dumps(result.result_set, default=str, ensure_ascii=False, indent=2)
        return json_client, 200, {'content-type': 'text/json'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400

if __name__ == '__main__':
    app.run(port=8000, host='127.0.0.1')

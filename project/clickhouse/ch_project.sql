drop table if exists client;
drop table if exists info_calls;

create table client
(
    id       int,
    name     text,
    age      int,
    phone    text,
    address  text,
    birthday text,
    email    text,
    -- массив вложенных объектов:
    "family.name" Array(text),
    "family.age" Array(int),
    "family.phone" Array(text),
    "family.family_ties" Array(text)
)
    engine = ReplacingMergeTree() order by (id); -- phone является так же первичным ключом, по которому сборщик мусора будет удалять старые записи

create table info_calls
(
    count_of_calls    int,
    avg_call_duration text,
    date_call         text,
    client_category   text,
    digitized_text    text
) engine = Memory();

insert into client (id, name, age, phone, address, birthday, email, "family.name", "family.age", "family.phone",
                    "family.family_ties")
values (1, 'Anastasia', 19, '8910540942', 'Moscow', '03.04.2003', 'nastya.zharkova.03@bk.ru', ['Елена', 'Никита'],
        [39, 16], ['89802345162', '89204765324'], ['Мать', 'Брат']),
       (2, 'Katya', 19, '89125437865', 'Sochi', '28.11.2003', 'ekaterina03@gmail.com', ['Дмитрий', 'Олеся'], [35, 12],
        ['89623452176', '89345672132'], ['Отец', 'Сестра']),
       (3, 'Egor', 22, '89567841745', 'Volgograd', '11.06.1999', 'e_gor2000@yandex.ru', ['Светлана', 'Александр'],
        [30, 21], ['89783674523', '89125673908'], ['Тётя', 'Брат']),
       (4, 'Lera', 20, '89106739257', 'Sochi', '15.02.2004', 'lera.0206@mail.ru', ['Татьяна', 'Игорь'], [65, 68],
        ['89324107843', '89234756721'], ['Бабушка', 'Дедушка']),
       (5, 'Arina', 35, '89564447211', 'Kaluga', '11.07.2003', 'arina_bivsheva19@gmail.com', ['Игорь', 'Кристина'],
        [40, 9], ['89154362745', '89104387643'], ['Муж', 'Дочь']),
       (6, 'Sasha', 55, '89153450906', 'Moscow', '29.06.2004', 'a_aleksandr2001@bk.ru', ['Анастасия', 'Олег'], [35, 12],
        ['89564736209', '89576847531'], ['Жена', 'Сын']);

insert into info_calls(count_of_calls, avg_call_duration, date_call, client_category, digitized_text)
values (4, 60, '01.11.2021', 'Родственник', 'Привет! Во сколько сегодня встретимся? - Давай в 4'),
       (2, 60, '01.11.2021', 'Родственник', 'Где продают ту вещь, про которую ты мне вчера говорил?'),
       (5, 120, '11.06.2018', 'Родственник', 'Где продают ту вещь, про которую ты мне вчера говорил?'),
       (1, 200, '28.02.2020', 'Друг', 'Как мне найти человека который точно знает про это всё'),
       (6, 50, '13.05.2021', 'Коллега', 'Добрый вечер! Когда я могу получить свой товар? -Здравствуйте! Сегодня в 23.00'),
       (7, 120, '12.07.2020', 'Друг', 'Привет! Когда мы с тобой уже пойдём в кино???'),
       (1, 80, '13.07.2020', 'Коллега', 'Добрый день! Когда будет готов отчёт по проделанной работе?');

select *
from client
where has("family.phone", '89802345162');

select max("family.age") as max_age
from client
         array join "family.age";

insert into client (name, age, phone, address, birthday, email, "family.name", "family.age", "family.phone",
                    "family.family_ties")
values ('Anastasia', 19, '8910540942', 'Moscow', '03.04.2003', 'nastya.zharkova.03@bk.ru', ['Елена', 'Никита', 'Юля'],
        [39, 16, 11], ['89802345162', '89204765324', '89103452634'], ['Мать', 'Брат', 'Сестра']);

select *
from client final;

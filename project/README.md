# Проектная работа

В папке создать файл **.env**, добавить туда:
```
APP_NAME=my-app
APP_PORT=27574

POSTGRES_USER=postgres
POSTGRES_PASSWORD=postgres
POSTGRES_PORT=54765
POSTGRES_DB=postgres

SLAVE_HOST=host.docker.internal
SLAVE_PORT=38749

REDIS_PASSWORD=redispglaba2022
REDIS_PORT=26596

ELASTIC_PORT=41565

CLICKHOUSE_USER=clickhouse
CLICKHOUSE_PASSWORD=clickhouse
CLICKHOUSE_PORT=53666
```

В папке replica создать файл **.env**, добавить туда:
```
SLAVE_PORT=38749

MASTER_PORT=999
MASTER_HOST=host.docker.internal
MASTER_PASSWORD=postgres
```

Запуск проекта:
```
docker compose up
```

Когда контейнеры запустятся, скопировать в адресную строку браузера:
```
localhost:27574
```

Чтобы проверить, что всё работает, скопируйте из файла "requests" запросы и выполните их в нужном месте.

---

Запросы Elastic выполнять в Postman.

---


Для проверки AutoComplete скопировать в браузер
```
http://127.0.0.1:27574/
```

и ввести в поисковую строку нужный запрос(например, "сегодня")